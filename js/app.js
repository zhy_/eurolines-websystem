/*jshint multistr: true */
  var app = {
    init: function() {
      this.stickNav();
    },

    stickNav: function() {
      if ($(window).width() < 1280) {
        $('.js-nav--main').addClass('nav--sticky');
      }
      else {
        $('.js-nav--main').removeClass('nav--sticky');
      }
    },

  };
  
  /**
   * Run application script
   */
  $( document ).ready( function() {
    app.init();
    $(window).resize(function() {
      app.stickNav();
    });
  });