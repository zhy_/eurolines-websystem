<?php $auth = true; ?>
<?php if($auth): ?>
<?php include_once('partials/header.php'); ?>
<?php include_once('partials/nav.php'); ?>

<div id="page-wrapper">

<?php  
  $page = isset($_GET['page']) ? $_GET['page'] : 'pages/open-shipments.html';
  include_once($page);
?>

</div>
<!-- /#page-wrapper -->


<?php include_once('partials/footer.php'); ?>
<?php 
else: 
  $page = 'pages/login.html';
  include_once($page);
endif;
?>