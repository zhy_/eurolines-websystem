
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top js-navbar--main navbar--main" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="index.php?page=pages/index.html">Euroline System GmbH</a>

    <ul class="nav navbar-top-links navbar-right navbar--switchers">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-flag-o fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-language">
          <li class="active"><a href="#"><i class="fa fa-flag fa-fw"></i> English</a></li>
          <li><a href="#"><i class="fa fa-flag fa-fw"></i> Danish</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
          <li><a href="index.php?page=pages/update-profile.html"><i class="fa fa-user fa-fw"></i> User Profile</a>
          </li>
          <li class="divider"></li>
          <li><a href="index.php?page=pages/login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
          </li>
        </ul>
      </li>
    </ul>
    <!-- /.navbar-top-links -->
    
  </div>
  <!-- /.navbar-header -->

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      
      

     <!--  <li>
        <a href="index.php?page=pages/index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
      </li>
      <li>
        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="index.php?page=pages/flot.html">Flot Charts</a>
          </li>
          <li>
            <a href="index.php?page=pages/morris.html">Morris.js Charts</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="index.php?page=pages/tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
      </li>
      <li>
        <a href="index.php?page=pages/forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
      </li>
      <li>
        <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="index.php?page=pages/panels-wells.html">Panels and Wells</a>
          </li>
          <li>
            <a href="index.php?page=pages/buttons.html">Buttons</a>
          </li>
          <li>
            <a href="index.php?page=pages/notifications.html">Notifications</a>
          </li>
          <li>
            <a href="index.php?page=pages/typography.html">Typography</a>
          </li>
          <li>
            <a href="index.php?page=pages/icons.html"> Icons</a>
          </li>
          <li>
            <a href="index.php?page=pages/grid.html">Grid</a>
          </li>
        </ul>
      </li> -->
    



      <li>
        <a href="login.php"><i class="fa fa-user fa-fw"></i> Login</a>
      </li>
      <li>
        <a href="index.php?page=pages/open-shipments.html"><i class="fa fa-sitemap fa-fw"></i> Open Shipments</a>
      </li>
      <li>
        <a href="index.php?page=pages/my-addresses.html"><i class="fa fa-map-marker fa-fw"></i> My Addresses</a>
      </li>
      <li>
        <a href="index.php?page=pages/create-booking.html"><i class="fa fa-plus fa-fw"></i> Create booking</a>
      </li>
      <li class="active">
        <a href="#"><i class="fa fa-list-ul fa-fw"></i> All pages<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="index.php?page=pages/open-shipments.html"><i class="fa fa-sitemap fa-fw"></i> Open Shipments</a>
          </li>
          <li>
            <a href="index.php?page=pages/my-addresses.html"><i class="fa fa-map-marker fa-fw"></i> My Addresses</a>
          </li>
          <li>
            <a href="index.php?page=pages/create-booking.html"><i class="fa fa-plus fa-fw"></i> Create booking</a>
          </li>
          <li>
            <a href="index.php?page=pages/dangerous-goods.html"><i class="fa fa-warning fa-fw"></i> Dangerous Goods</a>
          </li>
          <li>
            <a href="index.php?page=pages/shipment-information.html"><i class="fa fa-question-circle fa-fw"></i> Shipment Information</a>
          </li>
          <li>
          <a href="index.php?page=pages/shipment-information-2.html"><i class="fa fa-question-circle fa-fw"></i> Shipment Information 2</a>
          </li>
          <li>
            <a href="index.php?page=pages/shipment-status.html"><i class="fa fa-check-circle fa-fw"></i> Shipment Status</a>
          </li>
          <li>
            <a href="index.php?page=pages/update-profile.html"><i class="fa fa-user fa-fw"></i> Update Profile</a>
          </li>
        </ul>
        <!-- /.nav-second-level -->
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>
