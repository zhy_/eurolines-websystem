$(function() {

  $('#side-menu').metisMenu();
  $('.datepicker__input').datepicker({
    format: 'dd-mm-yyyy'
  });
  $(".js-sortable-table").tablesorter( {dateFormat: 'pt'} ); 
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
  $(window).bind("load resize", function() {
    topOffset = 50;
    width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
    // if (width < 768) {
    if (width < 1280) {
      $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
          } else {
            $('div.navbar-collapse').removeClass('collapse');
          }

          height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
          height = height - topOffset;
          if (height < 1) height = 1;
          if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
          }

          if ($(window).width() < 1280) {
            $('.js-navbar--main').addClass('navbar--sticky');
            $('.js-page-wrapper').css('padding-top', $('.js-navbar--main > .navbar-header').outerHeight());
          }
          else {
            $('.js-navbar--main').removeClass('navbar--sticky');
            $('.js-page-wrapper').attr('style', '');
          }

        });

  var url = window.location;
  var element = $('ul.nav a').filter(function() {
    return this.href == url || url.href.indexOf(this.href) == 0;
  }).addClass('active').parent().parent().addClass('in').parent();
  if (element.is('li')) {
    element.addClass('active');
  }
});
